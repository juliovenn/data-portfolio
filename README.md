
# Welcome to My Data Analysis and Science Portfolio! 🌟

Hello there! 👋 I'm thrilled to share my journey through data analysis and science with you. This portfolio is a collection of projects that showcase my skills, curiosity, and passion for uncovering insights and telling stories through data. Dive in to explore the magic of data with me!

## Directory Tree 📁

```
data-portfolio
|-- Data\ Analysis
|   |-- call-centers-politicos
|   |-- first-quantum
|   |-- planilla-MEDUCA
|   `-- planillas-gobierno-central
`-- Data\ Science
    |-- Supervised\ Learning
    `-- Unsupervised\ Learning
```

## Projects Overview 🚀

### Data Analysis

#### 📞 Call Centers Politicos

Dive into the world of politics through Instagram! In this project, I explored an Instagram post by a presidential candidate from Panama, analyzing comments to gauge public sentiment. Using OpenAI's API, I performed sentiment analysis to sift through the noise and highlight positive comments. But there's a twist! I also embarked on a digital detective mission to uncover fake accounts among the supporters. It's data analysis meets social media sleuthing!

#### 💎 First Quantum

In the wake of Panama's protests against First Quantum Minerals, this project offers a deep dive into the company's reported revenues from various mining projects. The analysis sheds light on a startling discovery - the absence of tax reports to the Panamanian government. Through meticulous data examination, this project reveals the economic impact and raises questions about corporate responsibility.

#### 📚 Planilla MEDUCA

Ever wondered how the Panamanian government spends its money on education? This project provides a detailed analysis of the Education Ministry's payroll, including contract types, numbers, and monthly expenses. It's a revealing look at the financial flows within one of the country's crucial sectors.

#### 🏛️ Planillas Gobierno Central

Taking a broader view, this project extends the payroll analysis to encompass all central government institutions in Panama. By comparing various departments, it offers insights into government spending and employment patterns, painting a comprehensive picture of public sector finances.

### Data Science


#### 🧠 Supervised Learning

In this section, I delve into the implementation of supervised learning techniques. Supervised learning involves training a model on labeled data, where the input features and corresponding output labels are known. The process begins with data preprocessing, which includes handling missing values, encoding categorical variables, and normalizing numerical features. 

Next, I split the dataset into training and testing sets to evaluate model performance. I experimented with various regression algorithms such as Linear Regression, Decision Trees, and Random Forests. Model selection was guided by cross-validation techniques to ensure robustness. 

Hyperparameter tuning was performed using grid search and random search methods to optimize model performance. The final model's accuracy was assessed using metrics like Root Mean Squared Error (RMSE) and R-squared (R²). This comprehensive approach ensures the development of a reliable predictive model.

#### 🔍 Unsupervised Learning

Unsupervised learning techniques are employed to uncover hidden patterns in data without predefined labels. The process starts with exploratory data analysis to understand the dataset's structure and distribution. Feature engineering is crucial to enhance the quality of the input data.

I utilized clustering algorithms such as K-means and hierarchical clustering to segment the data into meaningful groups. The choice of the number of clusters was determined using methods like the Elbow Method and Silhouette Analysis. 

Dimensionality reduction techniques like Principal Component Analysis (PCA) were applied to visualize high-dimensional data effectively. The results of clustering were validated through internal validation metrics such as Silhouette Score and Davies-Bouldin Index. This methodology facilitates the discovery of actionable insights from complex datasets.

## About Me 🙋‍♂️

Hey there! I'm Julio Orrego, a 26-year-old data aficionado with a Bachelor's degree in Computer Science and a Master's Degree in Analytics, Innovation, and Technology from INCAE Business School. My journey with databases began in 2015, and it was love at first query. This passion led me to pursue further studies in analytics and continually enhance my skills in data science with Python. I thrive on challenges and enjoy turning complex data into compelling stories.

## Let's Connect! 🌐

Got a data puzzle? Looking for a collaborator? Or simply want to chat about the latest in data science? I'm all ears!

- **Email:** [julio2897@gmail.com](mailto:julio2897@gmail.com)
- **LinkedIn:** [linkedin.com/in/julio-orrego-090374135/](https://www.linkedin.com/in/julio-orrego-090374135/)

Let's explore the endless possibilities of data together! 🚀

